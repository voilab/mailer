#!/bin/bash

# Attends le montage des fichiers sur la VM.
while [ ! -f /vagrant/vagrant-bootstrap.sh ]; do
	sleep 1
done

# Map des fichiers
rm -Rf /var/www/localhost/htdocs
ln -sfn /vagrant /var/www/localhost/htdocs

# Config php
sed -i -re 's#^(exec /usr/bin/php.+)$#\1 -d error_reporting="E_ALL \& ~E_DEPRECATED" #i' /var/www/bin/vagrant/php-wrapper

# Lancement de Mariadb
/etc/init.d/mysql start
rc-update add mysql default

# Lancement de Apache
sed -i -re 's/after sshd/after sshd virtualbox-guest-additions/i' /etc/init.d/apache2
sed -i -re 's#/var/www/localhost/htdocs/logs/access_log#/var/log/apache2/access_log#i' /etc/apache2/httpd.conf
sed -i -re 's#/var/www/localhost/tools/logs/access_log#/var/log/apache2/access_log#i' /etc/apache2/httpd.conf
/etc/init.d/apache2 start
rc-update add apache2 default

npm update -g npm
npm install -g gulp

cd /vagrant
sudo -u vagrant npm install
sudo -u vagrant composer update

echo
echo '---------------------------------------'
echo '| Voilab mailer                       |'
echo '| http://127.0.0.1:3939               |'
echo '---------------------------------------'
