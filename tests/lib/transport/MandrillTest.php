<?php

namespace voilab\mailer\test\transport;

use voilab\mailer\test\TransportTestCase;
use voilab\mailer\adapter;

class MandrillTest extends TransportTestCase {

    public function setUp() {
        $mandrill = new \Mandrill(MANDRILL_KEY);
        $this->init(function () use ($mandrill) {
            $adapter = new adapter\Mandrill($mandrill);
            return $adapter->setFrom(MAIL);
        });
    }

    public function testSend() {
        if (!MANDRILL_SEND) {
            $this->assertTrue(true);
            return;
        }
        $adapter = $this->mailer->create()
            ->addTo(MAIL)
            ->setSubject('mandrill 2 html')
            ->setHtml('some <strong>strong</strong> text {{data}}')
            ->setGlobalData([
                'data' => 'variable'
            ]);

        $this->mailer->send($adapter);
        $this->assertTrue(true);
    }
}
