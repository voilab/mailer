<?php

namespace voilab\mailer\test\transport;

use voilab\mailer\test\TransportTestCase;
use voilab\mailer\adapter;

class Sendgrid5Test extends TransportTestCase {

    public function setUp() {
        $grid = new \SendGrid(SENDGRID5_KEY);
        $this->init(function () use ($grid) {
            $adapter = new adapter\Sendgrid5($grid);
            return $adapter->setFrom(MAIL);
        });
    }

    public function testSend() {
        if (!SENDGRID5_SEND) {
            $this->assertTrue(true);
            return;
        }
        $adapter = $this->mailer->create()
            ->addTo(MAIL)
            ->setSubject('sendgrid 2 html')
            ->setHtml('some <strong>strong</strong> text %data%')
            ->setGlobalData([
                'data' => 'variable'
            ]);

        $this->mailer->send($adapter);
        $this->assertTrue(true);
    }
}
