<?php

namespace voilab\mailer\test\transport;

use voilab\mailer\test\TransportTestCase;
use voilab\mailer\adapter;

class Sparkpost2Test extends TransportTestCase {

    public function setUp() {
        $client = new \Http\Adapter\Guzzle6\Client(new \GuzzleHttp\Client());
        $sparkpost = new \SparkPost\SparkPost($client, [
            'key' => SPARKPOST2_KEY
        ]);
        $this->init(function () use ($sparkpost) {
            $adapter = new adapter\Sparkpost2($sparkpost);
            return $adapter->setFrom(MAIL);
        });
    }

    public function testSend() {
        if (!SPARKPOST2_SEND) {
            $this->assertTrue(true);
            return;
        }
        $adapter = $this->mailer->create()
            ->addTo(MAIL)
            ->setSubject('sparkpost 2 html')
            ->setHtml('some <strong>strong</strong> text {{data}}')
            ->setGlobalData([
                'data' => 'variable'
            ]);

        $this->mailer->send($adapter);
        $this->assertTrue(true);
    }
}
