<?php

namespace voilab\mailer\test\transport;

use voilab\mailer\test\TransportTestCase;
use voilab\mailer\adapter;

class Zend3Test extends TransportTestCase {

    public function setUp() {
        $smtp = new \Zend\Mail\Transport\Smtp(
            new \Zend\Mail\Transport\SmtpOptions([
                'name' => 'smtp',
                'host' => SMTP_HOST,
                'connection_class' => 'login',
                'connection_config' => [
                    'username' => SMTP_LOGIN,
                    'password' => SMTP_PASSWORD,
                    'ssl' => 'tls'
                ]
            ])
        );
        $this->init(function () use ($smtp) {
            $adapter = new adapter\Zend3($smtp);
            return $adapter->setFrom(MAIL);
        });
    }

    public function testSend() {
        if (!ZEND3_SEND) {
            $this->assertTrue(true);
            return;
        }
        $adapter = $this->mailer->create()
            ->addTo(MAIL)
            ->setSubject('zend 3 html')
            ->setHtml('some <strong>strong</strong> text');

        $this->mailer->send($adapter);
        $this->assertTrue(true);
    }
}
