<?php

namespace voilab\mailer\test;

use PHPUnit\Framework\TestCase;
use voilab\mailer\Transport;
use voilab\mailer\Renderer;
use voilab\mailer\adapter;

/**
 * Base testcase class for Mailer renderer test classes
 */
abstract class RendererTestCase extends TestCase {

    protected $mailer;

    public function init(Renderer $renderer) {
        $this->mailer = new Transport();
        $this->mailer->setAdapterFn(function () {
            return new adapter\Zend2(new \Zend\Mail\Transport\Sendmail());
        });
        $this->mailer->setRenderer($renderer);
    }
}
