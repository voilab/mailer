<?php
/*
 * This file bootstraps the test environment.
 */
namespace voilab\mailer\test;

define('TPL_PATH', __DIR__ . '/../tpls');
define('CACHE_PATH', __DIR__ . '/../cache');

error_reporting(E_ALL | E_STRICT);
date_default_timezone_set('UTC');

if (file_exists(__DIR__ . '/../../vendor/autoload.php')) {
    // dependencies were installed via composer - this is the main project
    $classLoader = require __DIR__ . '/../../vendor/autoload.php';
} elseif (file_exists(__DIR__ . '/../../../../autoload.php')) {
    // installed as a dependency in `vendor`
    $classLoader = require __DIR__ . '/../../../../autoload.php';
} else {
    throw new \Exception('Can\'t find autoload.php. Did you install dependencies via composer?');
}

if (!@file_exists(CACHE_PATH . '/twig') && !@mkdir(CACHE_PATH . '/twig', 0777, true)) {
    $e = error_get_last();
    throw new \Exception('Could not create ' . CACHE_PATH . '/twig Folder. ' . $e['message']);
}
