<?php

namespace voilab\mailer\test;

use PHPUnit\Framework\TestCase;
use voilab\mailer\Transport;
use voilab\mailer\Adapter;

abstract class TransportTestCase extends TestCase {

    /**
     * Mailer object
     * @var Transport
     */
    protected $mailer;

    public function init(callable $callable) {
        $this->mailer = new Transport();
        $this->mailer->setAdapterFn($callable);
    }
}
