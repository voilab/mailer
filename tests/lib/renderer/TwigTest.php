<?php

namespace voilab\mailer\test\renderer;

use voilab\mailer\test\RendererTestCase;
use voilab\mailer\renderer;

class TwigTest extends RendererTestCase {

    public function setUp() {
        $loader = new \Twig_Loader_Filesystem(TPL_PATH . '/twig');
        $twig = new \Twig_Environment($loader, [
            'cache' => CACHE_PATH . '/twig',
            'debug' => true
        ]);
        $renderer = new renderer\Twig($twig);
        $this->init($renderer);
    }

    public function testRender() {
        $adapter = $this->mailer->create()
            ->setTemplate('test.twig')
            ->setGlobalData([
                'subject' => 'title',
                'body' => 'content'
            ]);

        list($subject, $body) = $this->mailer->getRenderer()->render($adapter);
        $this->assertEquals($subject, 'SUBJECT title');
        $this->assertEquals($body, "<div>HEADER</div>\n\n<p>BODY content</p>\n<div>FOOTER</div>");
    }
}
