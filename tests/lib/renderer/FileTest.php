<?php

namespace voilab\mailer\test\renderer;

use voilab\mailer\test\RendererTestCase;
use voilab\mailer\renderer;

class FileTest extends RendererTestCase {

    public function setUp() {
        $renderer = new renderer\File(TPL_PATH . '/file', [
            'subject' => 'title'
        ]);
        $this->init($renderer);
    }

    public function testRender() {
        $adapter = $this->mailer->create()
            ->setTemplate('test.php')
            ->setGlobalData([
                'subject' => 'title',
                'body' => 'content'
            ]);

        list($subject, $body) = $this->mailer->getRenderer()->render($adapter);
        $this->assertEquals($subject, 'SUBJECT title');
        $this->assertEquals($body, '<p>BODY content</p>');
    }

    public function testRenderStandardParams() {
        $adapter = $this->mailer->create()
            ->setTemplate('test.php')
            ->setGlobalData([
                'body' => 'content'
            ]);

        list($subject, $body) = $this->mailer->getRenderer()->render($adapter);
        $this->assertEquals($subject, 'SUBJECT title');
        $this->assertEquals($body, '<p>BODY content</p>');
    }
}
