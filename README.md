# PHP Voilab Mailer

Transactional mailer

## Basic usage

### Create the main mailer transport object

```
use voilab\mailer\Transport;
use voilab\mailer\adapter;
use voilab\mailer\renderer;
```
```
$mailer = new Transport();
$mailer->setAdapterFn(function (Transport $mail, array $createParams) {
    return new adapter\SomeAdapter();
});
```

### Stuff the mail with data...

```
$adapter = $mailer
    ->create(['customParam' => 1])
    ->setFrom('from@email.com')
    ->addTo('to@email.com')
    ->setSubject('A subject from James')
    ->setHtml('<p>Hello John</p>');
```

### ... or use global datas, templates and renderers to introduce variations

Please check below for more explanations about renderers and templates.

```
$mailer->setRenderer(new renderer\File('/path'));
$adapter = $mailer
    ->create()
    ->setFrom('from@email.com')
    ->addTo('to@email.com')
    ->setTemplate('some-file.php')
    ->setGlobalData([
        'me' => 'James',
        'you' => 'John'
    ]);
```
```
<!-- /path/some-file.php -->
<h1 data-renderer-subject>A subject from <?= $me; ?></h1>
<p>Hello <?= $you; ?></p>
```

### Send the mail

```
try {
    $mailer->send($adapter);
    echo "mail is sent";
} catch (\Exception $e) {
    echo $e->getMessage();
}
```

## Templates

When using templates, subjects, html or text are ignored. They are all inside
the template. You need to set a template ID in the adapter:

```
$adapter->setTemplate('some-template-id');
```

> Note that if you use network templates, like with Sparkpost or Sendgrid, you
> can mix inline html with variables. The above restriction applies only when
> you use renderers (with Twig, for example).

With templates and renderers, you set the content automatically right before the
email is sent. You can set html or text content at anytime using these methods:

```
$adapter = $mailer
    ->setRenderer(new renderer\File('/path/files'))
    ->create();

$mailer
    ->setHtml($adapter, 'file_html.php')
    ->setText($adapter, 'file_text.php');

// then send the email
```

### File

File renderer is a fast and simple loader for PHP files. The global datas
you gave to your adapter are injected in the file (with *extract*) and you can
use them with raw PHP, includes, etc.

There are no check of any kind with variables. Use this templating mode at your
own risks.

```
$renderer = new renderer\File('/path/to/files');
$mailer->setRenderer($renderer);

$adapter = $mailer
    ->create()
    ->setTemplate('file.php');
```
```
<h1 data-renderer-subject>This is the mail subject  <?= $some_param; ?></h1>
<p>
    All the rest will be the mail body, with all html needed, and access to all
    params and functions from PHP.
</p>
<blockquote>
    The <strong>p</strong> tag is absolutely not required. You can start your
    message body (after the first h1) with anything you want, even an other h1
    or no html tag at all.
</blockquote>
```

### Twig

Twig renderer is more advanced as file renderer. You don't load a classic PHP
file, but a twig template. The global datas you gave to your adapter are used
as params when rendering the twig template.

```
$twig = new \Twig_Environment();
// configure your environment as you want

$renderer = new renderer\Twig($twig);
$mailer->setRenderer($renderer);

$mailer->create()->setTemplate('some_file.twig');
```
```
<h1 data-renderer-subject>This is the mail subject {{ some_param }}</h1>
<p>
    All the rest will be the mail body, with all html needed, and access to all
    params and functions from your <em>twig</em> environment.
</p>
<blockquote>
    The <strong>p</strong> tag is absolutely not required. You can start your
    message body (after the first h1) with anything you want, even an other h1
    or no html tag at all.
</blockquote>
```

Note that you'll need to add this dependency into your own `package.json`:

- `"twig/twig": "1.*"`

## Adapters

### Native mail function

To use native mail function, you can use the `Zend2` or `Zend3` adapter with the
right transport mechanism, as shown below.

```
$zend = new \Zend\Mail\Transport\Sendmail();

$mailer = new Transport();
$mailer->setAdapterFn(function () use ($zend) {
    return new adapter\Zend2($zend);
});
```

Note that you'll need to add these dependencies into your own `package.json`:

- `"zendframework/zend-mail": "2.*"`
- `"zendframework/zend-servicemanager": "3.*"`

### Zend Mail 3

```
// create the zend transport that fit your needs
$zend = new \Zend\Mail\Transport\Smtp(
    new \Zend\Mail\Transport\SmtpOptions([
        // smtp config
    ])
);

$mailer = new Transport();
$mailer->setAdapterFn(function () use ($zend) {
    return new adapter\Zend3($zend);
});
```

Note that you'll need to add these dependencies into your own `package.json`:

- `"zendframework/zend-mail": "2.*"`
- `"zendframework/zend-servicemanager": "3.*"`

### Zend Mail 2

```
// create the zend transport that fit your needs
$zend = new \Zend\Mail\Transport\Smtp(
    new \Zend\Mail\Transport\SmtpOptions([
        // smtp config
    ])
);

$mailer = new Transport();
$mailer->setAdapterFn(function () use ($zend) {
    return new adapter\Zend2($zend);
});
```

Note that you'll need to add these dependencies into your own `package.json`:

- `"zendframework/zend-mail": "2.*"`
- `"zendframework/zend-servicemanager": "3.*"`

### Sparkpost

```
$client = new \Http\Adapter\Guzzle6\Client(new \GuzzleHttp\Client());
$sparkpost = new SparkpostTransport($client, [
    'key' => 'api key'
]);

$mailer = new Transport();
$mailer->setAdapterFn(function () use ($sparkpost) {
    return new adapter\Sparkpost2($sparkpost);
});
```

Note that you'll need to add these dependencies into your own `package.json`:

- `"sparkpost/sparkpost": "2.*"`
- `"php-http/guzzle6-adapter": "1.*"`

### SendGrid

```
$client = new \SendGrid('api key');

$mailer = new Transport();
$mailer->setAdapterFn(function () use ($client) {
    return new adapter\Sendgrid5($client);
});
```

Note that you'll need to add this dependency into your own `package.json`:

- `"sendgrid/sendgrid": "5.*"`

### Mandrill

```
$client = new \Mandrill('api key');

$mailer = new Transport();
$mailer->setAdapterFn(function () use ($client) {
    return new adapter\Mandrill($client);
});
```

Note that you'll need to add this dependency into your own `package.json`:

- `"mandrill/mandrill": "1.*"`

## Tests

First, copy `tests/phpunit_default.xml` to `tests/phpunit.xml` and fill in the
right values. Then, in the vagrant ssh, simply do this:

```bash
cd /vagrant
./vendor/bin/phpunit -c tests/phpunit.xml
```

To let phpunit send mails, just set `true` in the corresponding constant in the
`phpunit.xml` file.
