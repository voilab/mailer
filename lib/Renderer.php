<?php

namespace voilab\mailer;

interface Renderer {

    /**
     * Render content (mail subject and body). The returned array needs to be:
     * [$subject, $body]
     *
     * @param Adapter $adapter
     * @return array [$subject, $body]
     */
    public function render(Adapter $adapter);

}
