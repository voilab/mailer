<?php

namespace voilab\mailer;

interface Adapter {

    /**
     * Add a recipient
     *
     * @param string $email
     * @param string $name
     * @param array $data
     * @return Adapter
     */
    public function addTo($email, $name = null, array $data = null);

    /**
     * Add a recipient Cc
     *
     * @param string $email
     * @param string $name
     * @param array $data
     * @return Adapter
     */
    public function addCc($email, $name = null, array $data = null);

    /**
     * Add a recipient Bcc
     *
     * @param string $email
     * @param string $name
     * @param array $data
     * @return Adapter
     */
    public function addBcc($email, $name = null, array $data = null);

    /**
     * Set from
     *
     * @param string $email
     * @param string $name
     * @return Adapter
     */
    public function setFrom($email, $name = null);

    /**
     * Set subject
     *
     * @param string $subject
     * @return Adapter
     */
    public function setSubject($subject);

    /**
     * Set HTML content
     *
     * @param string $html
     * @return Adapter
     */
    public function setHtml($html);

    /**
     * Set text content
     *
     * @param string $text
     * @return Adapter
     */
    public function setText($text);

    /**
     * Add an attachment to the mail
     *
     * @param string $content string representation of content
     * @param string $name file name
     * @param string $type file type
     * @param string $disposition attachment disposition
     * @return mixed the attachment object
     */
    public function addAttachment($content, $name, $type, $disposition = null);

    /**
     * Set all substitions variables in one shot
     *
     * @see addGlobalData
     * @param array $data key-value pairs
     * @return Adapter
     */
    public function setGlobalData(array $data);

    /**
     * Add substitution variable
     *
     * @param string $key
     * @param mixed $value
     * @return Adapter
     */
    public function addGlobalData($key, $value);

    /**
     * Return an array containing all global datas. MUST return an array.
     *
     * @return array
     */
    public function getGlobalData();

    /**
     * Add metadata information for the email
     *
     * @param string $key
     * @param mixed $value
     * @return Adapter
     */
    public function addMetaData($key, $value);

    /**
     * Add config information. Can be anything relevant for the adapter
     *
     * @param string $key
     * @param mixed $value
     * @param mixed $context
     * @return Adapter
     */
    public function addConfigData($key, $value, $context = null);

    /**
     * Set template id
     *
     * @param string $templateId
     * @return Adapter
     */
    public function setTemplate($templateId);

    /**
     * Return the template id
     *
     * @return string
     */
    public function getTemplate();

    /**
     * Remove all To, Cc and Bcc and metadatas associated to that
     *
     * @return Adapter
     */
    public function resetRecipients();

    /**
     * Send the email. Do not use it directely. Use the VoilabSend wrapper
     * instead.
     *
     * @return mixed
     */
    public function send();

}
