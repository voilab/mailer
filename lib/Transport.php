<?php

namespace voilab\mailer;

/**
 * @author voilab
 */
class Transport {

    /**
     * Email adapter function. Used to instanciate a new message adapter
     * @var callable
     */
    protected $adapterFn;

    /**
     * Content renderer. Used to manage variable replacement
     * @var Renderer
     */
    protected $renderer;

    /**
     * Mail and adapter configuration
     * @var array
     */
    protected $config;

    /**
     * Mailer contructor. The config array is formatted like this:
     * - debug (bool) true to set debug mode, default to false
     * - debugEmail (string) the email where all mails are sent, required in
     *   debug mode
     * - renderMode (string) html or text, default to html
     *
     * @param array $config parameters for transport vendor
     */
    public function __construct(array $config = []) {
        $this->config = array_merge([
            'debug' => false,
            'renderMode' => 'html'
        ], $config);
    }

    /**
     * Set an adapter function, used to mainpulate and send email
     *
     * @param callable $adapterFn
     * @return Transport
     */
    public function setAdapterFn(callable $adapterFn) {
        $this->adapterFn = $adapterFn;
        return $this;
    }

    /**
     * Returns a fresh new adapter instance. The callable method $adapterFn has
     * the main Mailer object as first argument
     *
     * @return Adapter
     */
    public function create(array $params = []) {
        $fn = $this->adapterFn;
        if (!$fn) {
            $msg = 'Unable to create an adapter. Please set an adapter function with setAdapterFn()';
            throw new Exception($msg);
        }
        $adapter = $fn($this, $params);
        if (!$adapter instanceof Adapter) {
            throw new Exception('Adapter function must return an Adapter object');
        }
        return $adapter;
    }

    /**
     * Set a renderer, used to manipulate variable replacement in content
     *
     * @param Renderer $renderer
     * @return Transport
     */
    public function setRenderer(Renderer $renderer) {
        $this->renderer = $renderer;
        return $this;
    }

    /**
     * Returns the configured renderer
     *
     * @return Renderer
     */
    public function getRenderer() {
        return $this->renderer;
    }

    /**
     * Return configuration object
     *
     * @return array
     */
    public function getConfig() {
        return $this->config;
    }

    /**
     * Get a config value (null if config doesn't exist). Use dot in key to
     * search deeper in the config array
     *
     * @param string $key
     * @return mixed null if config not found
     */
    public function conf($key) {
        $parts = explode('.', $key);
        $config = $this->config;
        while (count($parts)) {
            $part = array_shift($parts);
            if (isset($config[$part])) {
                $config = $config[$part];
            } else {
                return null;
            }
        }
        return $config;
    }

    /**
     * Check if in debug mode. Debug mode simply remove all recipients (to, cc
     * and bcc) and replace them with one custom email for tests
     *
     * @return bool
     */
    public function isDebug() {
        return $this->conf('debug');
    }

    /**
     * Set html from a renderer without sending the email next
     *
     * @param Adapter $adapter
     * @param string $tpl
     * @return Transport
     */
    public function setHtml(Adapter $adapter, $tpl) {
        return $this->setContent($adapter, $tpl, 'html');
    }

    /**
     * Set text from a renderer without sending the email next
     *
     * @param Adapter $adapter
     * @param string $tpl
     * @return Transport
     */
    public function setText(Adapter $adapter, $tpl) {
        return $this->setContent($adapter, $tpl, '');
    }

    /**
     * Send a mail
     *
     * @param Adapter $adapter
     * @return mixed
     */
    public function send(Adapter $adapter) {
        if ($this->isDebug()) {
            $adapter->resetRecipients();
            if (!$this->conf('debugEmail')) {
                // no custom email is configurated. Throw an exception
                $msg = "Debug mode! You need to set [debugEmail] in the mailer config";
                throw new Exception($msg);
            }
            $adapter->addTo($this->conf('debugEmail'));
        }
        // if a renderer is set, fetch the content and set subject and body
        if ($this->renderer && $adapter->getTemplate()) {
            list($subject, $body) = $this->renderer->parse($adapter);
            // subject may not have been present in the template
            if ($subject) {
                $adapter->setSubject($subject);
            }
            if ($this->conf('renderMode') === 'html') {
                $adapter->setHtml($body);
            } else {
                $adapter->setText($body);
            }
        }
        return $adapter->send();
    }

    /**
     * Set html or text from a renderer without sending the email next
     *
     * @param Adapter $adapter
     * @param string $tpl
     * @param string $mode
     * @return Transport
     */
    private function setContent(Adapter $adapter, $tpl, $mode) {
        if (!$this->renderer) {
            return $this;
        }
        $adapter->setTemplate($tpl);
        list($subject, $body) = $this->renderer->parse($adapter);
        $adapter->setTemplate(null);

        if ($mode === 'html') {
            $adapter->setHtml($body);
        } else {
            $adapter->setText($body);
        }
        return $this;
    }

}
