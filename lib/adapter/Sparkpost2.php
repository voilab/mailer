<?php

namespace voilab\mailer\adapter;

use voilab\mailer\Adapter;
use SparkPost\SparkPost as SparkpostTransport;
use SparkPost\SparkPostResponse;

class Sparkpost2 implements Adapter {

    /**
     * SparkPost object
     * @var SparkpostTransport
     */
    protected $transport;

    /**
     * Main message config array
     * @var array
     */
    protected $message = [
        'substitution_data' => [],
        'metadata' => [],
        'recipients' => [],
        'cc' => [],
        'bcc' => [],
        'content' => [
            'attachments' => []
        ],
        'template' => null
    ];

    /**
     * Sparkpost adapter constructor
     *
     * @param SparkpostTransport $sparkpost the main sparkpost object
     * @param array $messageConfig
     */
    public function __construct(SparkpostTransport $sparkpost, array $messageConfig = []) {
        $this->setTransport($sparkpost);
        $this->message = array_merge_recursive($this->message, $messageConfig);
    }

    /**
     * Return the message configuration
     *
     * @return array
     */
    public function getMessage() {
        return $this->message;
    }

    /**
     * Get Sparkpost object
     *
     * @return SparkpostTransport
     */
    public function getTransport() {
        return $this->transport;
    }

    /**
     * Set Sparkpost object
     *
     * @param SparkpostTransport $transport
     * @return Sparkpost2
     */
    public function setTransport(SparkpostTransport $transport) {
        $this->transport = $transport;
        return $this;
    }

    /**
     * {@inheritDocs}
     * @return Sparkpost2
     */
    public function addTo($email, $name = null, array $datas = null) {
        $this->message['recipients'][] = [
            'address' => [
                'name' => $name,
                'email' => $email
            ],
            'substitution_data' => $datas
        ];
        return $this;
    }

    /**
     * {@inheritDocs}
     * @return Sparkpost2
     */
    public function addCc($email, $name = null, array $datas = null) {
        $this->message['cc'][] = [
            'address' => [
                'name' => $name,
                'email' => $email
            ],
            'substitution_data' => $datas
        ];
        return $this;
    }

    /**
     * {@inheritDocs}
     * @return Sparkpost2
     */
    public function addBcc($email, $name = null, array $datas = null) {
        $this->message['bcc'][] = [
            'address' => [
                'name' => $name,
                'email' => $email
            ],
            'substitution_data' => $datas
        ];
        return $this;
    }

    /**
     * {@inheritDocs}
     * @return Sparkpost2
     */
    public function setFrom($email, $name = null) {
        $this->message['content']['from'] = [
            'name' => $name,
            'email' => $email
        ];
        $this->message['content']['reply_to'] = $email;
        return $this;
    }

    /**
     * {@inheritDocs}
     * @return Sparkpost2
     */
    public function setSubject($subject) {
        $this->message['content']['subject'] = $subject;
        return $this;
    }

    /**
     * {@inheritDocs}
     * @return Sparkpost2
     */
    public function setHtml($html) {
        $this->message['content']['html'] = $html;
        return $this;
    }

    /**
     * {@inheritDocs}
     * @return Sparkpost2
     */
    public function setText($text) {
        $this->message['content']['text'] = $text;
        return $this;
    }

    /**
     * {@inheritDocs}
     * @return array
     */
    public function addAttachment($content, $name, $type, $disposition = null) {
        $part = [
            'type' => $type,
            'name' => $name,
            'data' => base64_encode($content)
        ];
        $this->message['content']['attachments'][] = $part;
        return $part;
    }

    /**
     * {@inheritDocs}
     * @return Sparkpost2
     */
    public function setGlobalData(array $data) {
        foreach ($data as $key => $value) {
            $this->addGlobalData($key, $value);
        }
        return $this;
    }

    /**
     * {@inheritDocs}
     * @return Sparkpost2
     */
    public function addGlobalData($key, $value) {
        $this->message['substitution_data'][$key] = $value;
        if (substr($value, 0, 4) === 'http') {
            $this->message['substitution_data']['html_' . $key] = str_replace(['http://', 'https://'], '', $value);
        }
        return $this;
    }

    /**
     * {@inheritDocs}
     */
    public function getGlobalData() {
        return $this->message['substitution_data'];
    }

    /**
     * {@inheritDocs}
     * @return Sparkpost2
     */
    public function addMetaData($key, $value) {
        $this->message['metadata'][$key] = $value;
        return $this;
    }

    /**
     * {@inheritDocs}
     * @return Sparkpost2
     */
    public function addConfigData($key, $value, $context = null) {
        $is_array = false;
        if (strpos($key, '[]') !== false) {
            $key = str_replace('[]', '', $key);
            $is_array = true;
        }
        if ($is_array) {
            if (!isset($this->message[$key])) {
                $this->message[$key] = [];
            }
            $this->message[$key][] = $value;
        } else {
            $this->message[$key] = $value;
        }
        return $this;
    }

    /**
     * {@inheritDocs}
     * @return Sparkpost2
     */
    public function setTemplate($templateId) {
        $this->message['template'] = $templateId;
        return $this;
    }

    /**
     * {@inheritDocs}
     */
    public function getTemplate() {
        return $this->message['template'];
    }

    /**
     * {@inheritDocs}
     * @return Sparkpost2
     */
    public function resetRecipients() {
        $this->message['recipients'] = [];
        $this->message['cc'] = [];
        $this->message['bcc'] = [];
        return $this;
    }

    /**
     * {@inheritDocs}
     * @return SparkPostResponse
     */
    public function send() {
        $cnf = $this->message;
        if ($cnf['template']) {
            unset($cnf['content']['subject']);
            unset($cnf['content']['html']);
            unset($cnf['content']['text']);
        }
        if (!count($cnf['cc'])) {
            unset($cnf['cc']);
        }
        if (!count($cnf['bcc'])) {
            unset($cnf['bcc']);
        }
        if (!count($cnf['metadata'])) {
            unset($cnf['metadata']);
        }
        $promise = $this->transport->transmissions->post($cnf);
        return $promise->wait();
    }
}
