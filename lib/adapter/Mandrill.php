<?php

namespace voilab\mailer\adapter;

use voilab\mailer\Adapter,
    Mandrill as MandrillTransport;

class Mandrill implements Adapter {

    use traits\Template;

    /**
     * Mandrill object
     * @var MandrillTransport
     */
    protected $transport;

    /**
     * Main message config array
     * @var array
     */
    protected $message = [
        'to' => [],
        'headers' => [],
        'global_merge_vars' => [],
        'merge_vars' => [],
        'metadata' => [],
        'tags' => [],
        'recipient_metadata' => [],
        'attachments' => [],
        'images' => [],
        'google_analytics_domains' => []
    ];

    /**
     * Background sending
     * @var bool
     */
    private $async = false;

    /**
     * Api IP pool name
     * @var string
     */
    private $ipPool = null;

    /**
     * Deferred sending (format YYYY-MM-DD HH:MM:SS)
     * @var string
     */
    private $sendAt = null;

    /**
     * Mc:edit content definition for a template
     * @var array
     */
    private $tplContent = [];

    /**
     * Mandrill adapter constructor
     *
     * @param MandrillTransport $madrill the main mandrill object
     * @param array $messageConfig
     */
    public function __construct(MandrillTransport $mandrill, array $messageConfig = []) {
        $this->setTransport($mandrill);
        $this->message = array_merge_recursive($this->message, $messageConfig);
    }

    /**
     * Return the message configuration
     *
     * @return array
     */
    public function getMessage() {
        return $this->message;
    }

    /**
     * Get Mandrill object
     *
     * @return MandrillTransport
     */
    public function getTransport() {
        return $this->transport;
    }

    /**
     * Set Mandrill object
     *
     * @param MandrillTransport $transport
     * @return Mandrill
     */
    public function setTransport(MandrillTransport $transport) {
        $this->transport = $transport;
        return $this;
    }

    /**
     * Set background sending
     *
     * @param bool $async
     * @return Mandrill
     */
    public function setAsync($async) {
        $this->async = (bool) $async;
        return $this;
    }

    /**
     * Set IP pool name
     *
     * @param string $name
     * @return Mandrill
     */
    public function setIpPool($name) {
        $this->ipPool = $name;
        return $this;
    }

    /**
     * Set deferred sending
     *
     * @param string $at
     * @return Mandrill
     */
    public function setSentAt($at) {
        $this->sendAt = $at;
        return $this;
    }

    /**
     * Add a mc:edit content config for the template
     *
     * @param string $name mc:edit region name
     * @param string $content
     * @return Mandrill
     */
    public function addTemplateContent($name, $content) {
        $this->tplContent[] = [
            'name' => $name,
            'content' => $content
        ];
        return $this;
    }

    /**
     * Get template mc:edit contents
     *
     * @return array
     */
    public function getTemplateContent() {
        return $this->tplContent;
    }

    /**
     * {@inheritDocs}
     * @return Mandrill
     */
    public function addTo($email, $name = null, array $datas = null) {
        return $this->addRecipient('to', $email, $name, $datas);
    }

    /**
     * {@inheritDocs}
     * @return Mandrill
     */
    public function addCc($email, $name = null, array $datas = null) {
        return $this->addRecipient('cc', $email, $name, $datas);
    }

    /**
     * {@inheritDocs}
     * @return Mandrill
     */
    public function addBcc($email, $name = null, array $datas = null) {
        return $this->addRecipient('bcc', $email, $name, $datas);
    }

    /**
     * {@inheritDocs}
     * @return Mandrill
     */
    public function setFrom($email, $name = null) {
        $this->message['from_email'] = $email;
        $this->message['from_name'] = $name;
        $this->message['headers']['Reply-To'] = $email;
        return $this;
    }

    /**
     * {@inheritDocs}
     * @return Mandrill
     */
    public function setSubject($subject) {
        $this->message['subject'] = $subject;
        return $this;
    }

    /**
     * {@inheritDocs}
     * @return Mandrill
     */
    public function setHtml($html) {
        $this->message['html'] = $html;
        return $this;
    }

    /**
     * {@inheritDocs}
     * @return Mandrill
     */
    public function setText($text) {
        $this->message['text'] = $text;
        return $this;
    }

    /**
     * {@inheritDocs}
     * @return array
     */
    public function addAttachment($content, $name, $type, $disposition = null) {
        $part = [
            'type' => $type,
            'name' => $name,
            'content' => base64_encode($content)
        ];
        $this->message['attachments'][] = $part;
        return $part;
    }

    /**
     * {@inheritDocs}
     * @return Mandrill
     */
    public function setGlobalData(array $data) {
        foreach ($data as $key => $value) {
            $this->addGlobalData($key, $value);
        }
        return $this;
    }

    /**
     * {@inheritDocs}
     * @return Mandrill
     */
    public function addGlobalData($key, $value) {
        $this->message['global_merge_vars'][] = [
            'name' => $key,
            'content' => $value
        ];
        return $this;
    }

    /**
     * {@inheritDocs}
     */
    public function getGlobalData() {
        return $this->message['global_merge_vars'];
    }

    /**
     * {@inheritDocs}
     * @return Mandrill
     */
    public function addMetaData($key, $value) {
        $this->message['metadata'][$key] = $value;
        return $this;
    }

    /**
     * {@inheritDocs}
     * @return Mandrill
     */
    public function addConfigData($key, $value, $context = null) {
        $is_array = false;
        if (strpos($key, '[]') !== false) {
            $key = str_replace('[]', '', $key);
            $is_array = true;
        }
        if ($is_array) {
            if (!isset($this->message[$key])) {
                $this->message[$key] = [];
            }
            $this->message[$key][] = $value;
        } else {
            $this->message[$key] = $value;
        }
        return $this;
    }

    /**
     * {@inheritDocs}
     * @return Mandrill
     */
    public function resetRecipients() {
        $this->message['to'] = [];
        $this->message['merge_vars'] = [];
        $this->message['recipient_metadata'] = [];
        $this->message['bcc_address'] = null;
        return $this;
    }

    /**
     * {@inheritDocs}
     * @return array Mandrill response array
     */
    public function send() {
        $async = $this->async;
        $pool = $this->ipPool;
        $at = $this->sendAt;
        if (!$this->getTemplate()) {
            $r = $this->transport->messages->send($this->message, $async, $pool, $at);
        } else {
            $tpl = $this->getTemplate();
            $tpl_content = $this->getTemplateContent();
            $r = $this->transport->messages->sendTemplate($tpl, $tpl_content, $this->message, $async, $pool, $at);
        }
        return $r;
    }

    /**
     * Add a recipient
     *
     * @param string $type to, cc or bcc
     * @param string $email
     * @param string $name
     * @param array $datas
     * @return array
     */
    private function addRecipient($type, $email, $name, array $datas = null) {
        $this->message['to'][] = [
            'name' => $name,
            'email' => $email,
            'type' => $type
        ];
        if ($datas) {
            $this->message['merge_vars'][] = [
                'rcpt' => $email,
                'vars' => $data
            ];
        }
        return $this;
    }
}
