<?php

namespace voilab\mailer\adapter;

use voilab\mailer\Exception;
use voilab\mailer\Adapter;
use SendGrid;

class Sendgrid5 implements Adapter {

    /**
     * Sendgrid object
     * @var SendGrid
     */
    protected $transport;

    /**
     * String surronding a globaldata key
     * @var string
     */
    protected $globalDataSurround;

    /**
     * Sendgrid adapter constructor
     *
     * @param SendGrid $sparkpost the main sparkpost object
     * @param string $globalDataSurround String surronding a globaldata key
     */
    public function __construct(SendGrid $transport, $globalDataSurround = '%') {
        $this->transport = $transport;
        $this->message = new SendGrid\Mail();
        $this->personalization = new SendGrid\Personalization();
        $this->globalDataSurround = $globalDataSurround;
    }

    /**
     * Return the message configuration
     *
     * @return SendGrid\Mail
     */
    public function getMessage() {
        return $this->message;
    }

    /**
     * Return the personalization configuration
     *
     * @return SenGrid\Personalization
     */
    public function getPersonalization() {
        return $this->personalization;
    }

    /**
     * Get Zend Transport object
     *
     * @return SendGrid
     */
    public function getTransport() {
        return $this->transport;
    }

    /**
     * {@inheritDocs}
     * @return Sendgrid5
     */
    public function addTo($email, $name = null, array $datas = null) {
        $obj = new SendGrid\Email($name, $email);
        $this->personalization->addTo($obj);
        return $this;
    }

    /**
     * {@inheritDocs}
     * @return Sendgrid5
     */
    public function addCc($email, $name = null, array $datas = null) {
        $obj = new SendGrid\Email($name, $email);
        $this->personalization->addCc($obj);
        return $this;
    }

    /**
     * {@inheritDocs}
     * @return Sendgrid5
     */
    public function addBcc($email, $name = null, array $datas = null) {
        $obj = new SendGrid\Email($name, $email);
        $this->personalization->addBcc($obj);
        return $this;
    }

    /**
     * {@inheritDocs}
     * @return Sendgrid5
     */
    public function setFrom($email, $name = null) {
        $obj = new SendGrid\Email($name, $email);
        $reply = new SendGrid\ReplyTo($email);
        $this->message->setFrom($obj);
        $this->message->setReplyTo($reply);
        return $this;
    }

    /**
     * {@inheritDocs}
     * @return Sendgrid5
     */
    public function setSubject($subject) {
        $this->message->setSubject($subject);
        return $this;
    }

    /**
     * {@inheritDocs}
     * @return Sendgrid5
     */
    public function setHtml($html) {
        $content = new SendGrid\Content('text/html', $html);
        $this->message->addContent($content);
        return $this;
    }

    /**
     * {@inheritDocs}
     * @return Sendgrid5
     */
    public function setText($text) {
        $content = new SendGrid\Content('text/plain', $text);
        $this->message->addContent($content);
        return $this;
    }

    /**
     * {@inheritDocs}
     * @return array
     */
    public function addAttachment($content, $name, $type, $disposition = null) {
        $obj = new SendGrid\Attachment();
        $obj->setContent($content);
        $obj->setType($type);
        $obj->setFilename($name);
        $obj->setDisposition($disposition ?: 'attachment');
        $this->message->addAttachment($obj);
        return $obj;
    }

    /**
     * {@inheritDocs}
     * @return Sendgrid5
     */
    public function setGlobalData(array $data) {
        foreach ($data as $key => $value) {
            $this->addGlobalData($key, $value);
        }
        return $this;
    }

    /**
     * {@inheritDocs}
     * @return Sendgrid5
     */
    public function addGlobalData($key, $value) {
        $key = $this->globalDataSurround . $key . $this->globalDataSurround;
        $this->personalization->addSubstitution($key, $value);
        return $this;
    }

    /**
     * {@inheritDocs}
     */
    public function getGlobalData() {
        return $this->personalization->getSubstitutions() ?: [];
    }

    /**
     * {@inheritDocs}
     * @return Sendgrid5
     */
    public function addMetaData($key, $value) {
        $this->personalization->addCustomArg($key, $value);
        return $this;
    }

    /**
     * {@inheritDocs}
     * @return Sendgrid5
     */
    public function addConfigData($key, $value, $context = null) {
        return $this;
    }

    /**
     * {@inheritDocs}
     * @return Sendgrid5
     */
    public function setTemplate($templateId) {
        $this->message->setTemplateId($templateId);
        return $this;
    }

    /**
     * {@inheritDocs}
     */
    public function getTemplate() {
        return $this->message->getTemplateId();
    }

    /**
     * {@inheritDocs}
     * @return Sendgrid5
     */
    public function resetRecipients() {
        $person = new SendGrid\Personalization();
        $p = $this->personalization;
        // there is no other way to reset private vars in the personalization
        // than to copy all others...
        $person->setSubject($p->getSubject());
        $person->setSendAt($p->getSendAt());
        $methods = [
            'getHeaders' => 'addHeader',
            'getSubstitutions' => 'addSubstitution',
            'getCustomArgs' => 'addCustomArg'
        ];
        foreach ($methods as $mk => $mv) {
            if ($p->$mk()) {
                foreach ($p->$mk() as $k => $v) {
                    $person->$mv($k, $v);
                }
            }
        }
        $this->personalization = $person;
        return $this;
    }

    /**
     * {@inheritDocs}
     * @return SendGrid\Response
     */
    public function send() {
        $this->message->addPersonalization($this->personalization);

        $response = $this->transport->client
            ->mail()
            ->send()
            ->post($this->message);

        if ($response->statusCode() >= 300 || $response->statusCode() < 200) {
            throw new Exception($response->body(), $response->statusCode());
        }
        return $response;
    }
}
