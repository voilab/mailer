<?php

namespace voilab\mailer\adapter;

use voilab\mailer\Adapter;
use Zend\Mail\Message;
use Zend\Mime\Mime;
use Zend\Mime\Part as MimePart;
use Zend\Mime\Message as MimeMessage;
use Zend\Mail\Transport;

class Zend3 implements Adapter {

    use traits\Template, traits\GlobalData, traits\ConfigData, traits\MetaData;

    /**
     * Zend Message
     * @var Message
     */
    protected $message;

    /**
     * Zend Mime message body
     * @var MimeMessage
     */
    protected $messageBody;

    /**
     * Zend Message Body
     * @var Transport\TransportInterface
     */
    protected $transport;

    /**
     * Charset encondig
     * @var string
     */
    protected $charset = 'utf8';

    /**
     * Zend Mail 2 adapter constructor
     */
    public function __construct(Transport\TransportInterface $transport, Message $message = null, MimeMessage $mimeMessage = null) {
        $this->setMessage($message ?: new Message());
        $this->setMessageBody($mimeMessage ?: new MimeMessage());
        $this->setTransport($transport);
    }

    /**
     * Get Zend message object
     *
     * @param string $charset
     * @return Zend2
     */
    public function setCharset($charset) {
        $this->charset = $charset;
        return $this;
    }

    /**
     * Get Zend message object
     *
     * @return Message
     */
    public function getMessage() {
        return $this->message;
    }

    /**
     * Set Zend Message
     *
     * @param Message $message
     * @return Zend2
     */
    public function setMessage(Message $message) {
        $this->message = $message;
        return $this;
    }

    /**
     * Get Zend Mime message body object
     *
     * @return MimeMessage
     */
    public function getMessageBody() {
        return $this->messageBody;
    }

    /**
     * Set Zend Mime Message body
     *
     * @param MimeMessage $message
     * @return Zend2
     */
    public function setMessageBody(MimeMessage $message) {
        $this->messageBody = $message;
        return $this;
    }

    /**
     * Get Zend Transport object
     *
     * @return Transport\TransportInterface
     */
    public function getTransport() {
        return $this->transport;
    }

    /**
     * Set Zend Transport object
     *
     * @param Transport\TransportInterface $transport
     * @return Zend2
     */
    public function setTransport(Transport\TransportInterface $transport) {
        $this->transport = $transport;
        return $this;
    }

    /**
     * {@inheritDocs}
     * @return Zend2
     */
    public function addTo($email, $name = null, array $data = null) {
        $this->getMessage()->addTo($email, $name);
        return $this;
    }

    /**
     * {@inheritDocs}
     * @return Zend2
     */
    public function addCc($email, $name = null, array $data = null) {
        $this->getMessage()->addCc($email, $name);
        return $this;
    }

    /**
     * {@inheritDocs}
     * @return Zend2
     */
    public function addBcc($email, $name = null, array $data = null) {
        $this->getMessage()->addBcc($email, $name);
        return $this;
    }

    /**
     * {@inheritDocs}
     * @return Zend2
     */
    public function setFrom($email, $name = null) {
        $this->getMessage()->setFrom($email, $name);
        $this->getMessage()->setReplyTo($email);
        return $this;
    }

    /**
     * {@inheritDocs}
     * @return Zend2
     */
    public function setSubject($subject) {
        $this->getMessage()->setSubject($subject);
        return $this;
    }

    /**
     * {@inheritDocs}
     * @return Zend2
     */
    public function setHtml($html) {
        return $this->setContent($html, Mime::TYPE_HTML);
    }

    /**
     * {@inheritDocs}
     * @return Zend2
     */
    public function setText($text) {
        return $this->setContent($text, Mime::TYPE_TEXT);
    }

    /**
     * {@inheritDocs}
     * @return MimePart
     */
    public function addAttachment($content, $name, $type, $disposition = null) {
        $part = new MimePart($content);
        $part->type = $type;
        $part->filename = $name;
        $part->disposition = $disposition ?: Mime::DISPOSITION_ATTACHMENT;
        $part->encoding = Mime::ENCODING_BASE64;

        $this->getMessageBody()->addPart($part);
        return $part;
    }

    /**
     * {@inheritDocs}
     * @return Zend2
     */
    public function resetRecipients() {
        $headers = $this->getMessage()->getHeaders();
        $headers->removeHeader('to');
        $headers->removeHeader('cc');
        $headers->removeHeader('bcc');
        return $this;
    }

    /**
     * {@inheritDocs}
     * @return mxied
     */
    public function send() {
        $msg = $this->getMessage();
        $body = $this->getMessageBody();

        foreach ($body->getParts() as $part) {
            if (!in_array($part->type, [Mime::TYPE_TEXT, Mime::TYPE_HTML])) {
                $msg->getHeaders()->addHeaderLine(
                    'Content-Type',
                    'multipart/related'
                );
                break;
            }
        }
        // because Mime use current($this->parts), with the foreach above, the
        // method return false
        $parts = $body->getParts();
        reset($parts);
        $body->setParts($parts);

        $msg->setEncoding($this->charset);
        $msg->setBody($body);

        return $this->getTransport()->send($msg);
    }

    /**
     * Set message body content
     *
     * @param string $content
     * @param string $type html or text
     * @return Zend2
     */
    private function setContent($content, $type) {
        $part = new MimePart($content);
        $part->type = $type;
        $part->charset = $this->charset;
        $part->encoding = Mime::ENCODING_QUOTEDPRINTABLE;

        $this->getMessageBody()->addPart($part);

        return $this;
    }
}
