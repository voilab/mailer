<?php

namespace voilab\mailer\adapter\traits;

trait MetaData {

    /**
     * {@inheritDocs}
     */
    public function addMetaData($key, $value) {
        return $this;
    }
}
