<?php

namespace voilab\mailer\adapter\traits;

trait GlobalData {

    /**
     * The global datas
     * @var array
     */
    protected $_globalDatas = [];

    /**
     * {@inheritDocs}
     */
    public function setGlobalData(array $data) {
        $this->_globalDatas = array_merge($this->_globalDatas, $data);
        return $this;
    }

    /**
     * {@inheritDocs}
     */
    public function addGlobalData($key, $value) {
        $this->_globalDatas[$key] = $value;
        return $this;
    }

    /**
     * {@inheritDocs}
     */
    public function getGlobalData() {
        return $this->_globalDatas;
    }
}
