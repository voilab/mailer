<?php

namespace voilab\mailer\adapter\traits;

trait Template {

    /**
     * The template id
     * @var string
     */
    protected $_tpl;

    /**
     * {@inheritDocs}
     */
    public function setTemplate($templateId) {
        $this->_tpl = $templateId;
        return $this;
    }

    /**
     * {@inheritDocs}
     */
    public function getTemplate() {
        return $this->_tpl;
    }
}
