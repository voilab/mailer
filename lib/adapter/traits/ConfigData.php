<?php

namespace voilab\mailer\adapter\traits;

trait ConfigData {

    /**
     * {@inheritDocs}
     */
    public function addConfigData($key, $value, $context = null) {
        return $this;
    }
}
