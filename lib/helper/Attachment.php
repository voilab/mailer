<?php

namespace voilab\mailer\helper;

use voilab\mailer\Adapter;
use voilab\mailer\Exception;

class Attachment {

    /**
     * Add an attachment to the mail from a local server file
     *
     * @param Adapter $adapter
     * @param array $files a $_FILES content, or formatted the same way
     * @param string $name file name, default to $file basename
     * @param string $type file type, default to auto detect
     * @param string $disposition attachment disposition
     * @return mixed the attachment object
     * @throws Exception
     */
    public static function addLocalFileAttachment(Adapter $adapter, $files, $name = null, $type = null, $disposition = null) {

        // many files
        if (is_array($files['tmp_name'])) {
            foreach ($files['tmp_name'] as $index => $file) {
                if (!file_exists($file)) {
                    throw new Exception("File " . $files['name'][$index] . " was not found on the server");
                }

                static::addAttachment($adapter, $file, $files['name'][$index], null, $disposition);
            }
            return $adapter;
        }

        // one single file
        return static::addAttachment($adapter, $files['tmp_name'], ($name ?: $files['name']), $type, $disposition);
    }

    protected static function addAttachment(Adapter $adapter, $file, $fileName, $fileType, $disposition) {
        $finfo = new \finfo(\FILEINFO_MIME_TYPE);
        return $adapter->addAttachment(
            file_get_contents($file),
            $fileName ?: basename($file),
            $fileType ?: $finfo->file($file),
            $disposition
        );
    }

    /**
     * Add an attachment to the mail from a buffer (like an url or $_FILES)
     *
     * @param Adapter $adapter
     * @param string $file file path and name or url
     * @param string $name file name, default to $file basename
     * @param string $type file type, default to auto detect
     * @param string $disposition attachment disposition
     * @return mixed the attachment object
     */
    public static function addBufferAttachment(Adapter $adapter, $file, $name = null, $type = null, $disposition = null) {
        $finfo = new \finfo(\FILEINFO_MIME_TYPE);
        return $adapter->addAttachment(
            file_get_contents($file),
            $name ?: basename($file),
            $type ?: $finfo->buffer($file),
            $disposition
        );
    }
}
