<?php

namespace voilab\mailer\renderer;

use Twig_Environment as TwigEnvironment;
use voilab\mailer\Renderer;
use voilab\mailer\Adapter;

class Twig implements Renderer {

    use traits\Util;

    /**
     * Twig environment
     * @var TwigEnvironment
     */
    private $twig;

    /**
     * Constructor
     *
     * @param TwigEnvironment $twig
     */
    public function __construct(TwigEnvironment $twig) {
        $this->setTwig($twig);
    }

    /**
     * Set twig object
     *
     * @param TwigEnvironment $twig
     * @return Twig
     */
    public function setTwig(TwigEnvironment $twig) {
        $this->twig = $twig;
        return $this;
    }

    /**
     * {@inheritDocs}
     */
    public function render(Adapter $adapter) {
        $content = $this->twig->render(
            $adapter->getTemplate(),
            $adapter->getGlobalData()
        );
        return $this->splitSubjectAndBody($content);
    }
}
