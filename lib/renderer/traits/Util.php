<?php

namespace voilab\mailer\renderer\traits;

trait Util {

    /**
     * Split subject and body from one same string. Split occurs on the 1st
     * <h1 data-renderer-subject></h1> found.
     *
     * @param string $content
     * @return array two key, 0 = subject and 1 = body
     */
    protected function splitSubjectAndBody($content) {
        $pattern = '/<h1 data-renderer-subject>(.*)<\/h1>/i';
        preg_match($pattern, $content, $matches);
        return [
            isset($matches[1]) ? $matches[1] : '',
            trim(preg_replace($pattern, '', $content, 1))
        ];
    }
}
