<?php

namespace voilab\mailer\renderer;

use voilab\mailer\Renderer;
use voilab\mailer\Adapter;
use voilab\mailer\Exception;

class File implements Renderer {

    use traits\Util;

    /**
     * Path to template files
     * @var string
     */
    private $path;

    /**
     * Default params available in the template file
     * @var array
     */
    private $params;

    /**
     * Constructor
     *
     * @param string $path path to template files
     * @param array $params standard params available in template files
     */
    public function __construct($path, array $params = []) {
        $this->setPath($path);
        $this->params = $params;
    }

    /**
     * Get path to template files
     *
     * @param string $path
     * @return Renderer
     */
    public function setPath($path) {
        $this->path = rtrim($path, ' /');
        return $this;
    }

    /**
     * Return path to template files
     * @return string
     */
    public function getPath() {
        return $this->path;
    }

    /**
     * {@inheritDocs}
     */
    public function render(Adapter $adapter) {
        $content = $this->getTpl(
            $adapter->getTemplate(),
            array_merge($this->params, $adapter->getGlobalData())
        );
        return $this->splitSubjectAndBody($content);
    }

    /**
     * Fetch a template content
     *
     * @param string $tpl template file name, without extension
     * @param array $params
     * @return string
     */
    protected function getTpl($tpl, array $params = []) {
        $file = $this->getPath() . '/' . ltrim($tpl, '/');
        if (!file_exists($file)) {
            throw new Exception(sprintf("Mail template [ %s ] doesn't exist", $tpl));
        }
        extract($params);
        ob_start();
        include $file;
        return ob_get_clean();
    }

}
